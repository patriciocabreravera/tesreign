const mongoose = require('mongoose')

let Schema = mongoose.Schema

let postSchema = new Schema({
    title: String,
    author: String,
    url: String,
    story_url: String,
    created_at: Date
});



module.exports = mongoose.model('Post',postSchema);