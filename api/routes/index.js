var express = require('express');
var router = express.Router();
const axios = require('axios');
const Post = require('../models/post')
/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Express'
  });
});

router.get('/seed_database', function (req, res, next) {

  let body = req.body;

  try {

    axios.get("http://hn.algolia.com/api/v1/search_by_date?query=nodejs")
      .then(response => {

        let posts = response.data.hits;
        posts.map(e => {

          let post = new Post({
            title: e.title,
            author: e.author,
            url: e.url,
            story_url: e.story_url,
            created_at: e.created_at
          })

          post.save((err, postDB) => {

            if (err) {

              return res.status(400).json({
                ok: false,
                err
              })

            }
          })

        });

        res.status(200).json({
          ok: true,
          message: "Base de datos cargada"
        })


      })
      .catch(err =>

        res.status(400).json({
          ok: false,
          err
        })

      );
  } catch (err) {

    res.status(400).json({
      ok: false,
      err
    })

  }

});

router.get('/clean_database', function (req, res, next) {

  Post.remove({},function(err,removed) {

    if(err)
      res.status(400).json({
        ok: false,
        err
      })

      //ok
      res.json({
        ok: false,
        message: "Se eliminaron todos los posts"
      })

 });

});

router.delete('/post/:id', function (req, res, next) {

  let id = req.params.id
  Post.findByIdAndRemove(id, (err, postBorrado) => {
    //si hay un error, 400 con el error
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      })
    }

    //si el usuario no se encuentra, devolver error
    if (!postBorrado) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'Post not found'
        }
      })
    }
    //ok
    res.json({
      ok: true,
      postBorrado
    })
  })

});

router.get('/post', function (req, res, next) {

  Post.find({},function(err,posts) {
    
    if(err)
      res.status(400).json({
        ok: false,
        err
      })
    
    res.status(200).json({
      ok: true,
      posts
    })

 });

});

module.exports = router;